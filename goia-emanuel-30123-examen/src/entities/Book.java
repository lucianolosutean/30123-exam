package entities;



public class Book extends Product  {


    @Override
    public void retireForSale() {
        this.setCategoryId(false);
    }

    @Override
    public void markForSale() {
        this.setForSale(true);
    }
}
