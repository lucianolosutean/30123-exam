package categories;

import entities.Product;

abstract public class Category {
    private int id;
    private String name;

    abstract public void addProduct(Product p);

    abstract public void removeProduct(Product p);

    abstract public void updateProduct(Product p);

    abstract Product getProduct(int id);
}
