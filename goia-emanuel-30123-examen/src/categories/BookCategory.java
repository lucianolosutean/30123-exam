package categories;

import entities.Book;
import entities.Product;

import java.util.ArrayList;

public class BookCategory extends Category {
    ArrayList<Book> books = new ArrayList<>();

    public void addProduct(Product p) {
        books.add((Book) p);
    }

    public void removeProduct(Product p) {
        books.remove((Book) p);
    }

    public void updateProduct(Product p) {
        books.set(books.indexOf(p),(Book)p);
    }

    Product getProduct(int id) {
        return books.get(id);
    }
}

