package categories;

import entities.Product;
import entities.Toy;

import java.util.ArrayList;

public class ToyCategory extends Category {
    ArrayList<Toy> toys = new ArrayList<>();

    @Override
    public void addProduct(Product p) {
        toys.add((Toy) p);
    }

    @Override
    public void removeProduct(Product p) {
        toys.remove((Toy) p);
    }

    @Override
    public void updateProduct(Product p) {
        toys.set(toys.indexOf(p),(Toy)p);
    }

    @Override
    Product getProduct(int id) {
        return toys.get(id);
    }
}
