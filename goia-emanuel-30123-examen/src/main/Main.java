package main;

import javax.swing.*;

public class Main extends JFrame {


    JMenuBar menubar;
    JMenu menu;

    JMenuItem a, b, c, d, e;

    public Main() {
        menubar = new JMenuBar();
        menu = new JMenu("Product");

        a = new JMenuItem("Aduga un produs nou in categorie");
        b = new JMenuItem("Stergere un produs din catogorie ");
        c = new JMenuItem("Editeaza un produs din categorie");
        d = new JMenuItem("Pune un produs la vanzare");
        e = new JMenuItem("Retrage un produs de la vanzare");

        menubar.add(menu);
        menu.add(a);
        menu.add(b);
        menu.add(c);
        menu.add(d);
        menu.add(e);
        setJMenuBar(menubar);

        a.addActionListener(e1 -> {
            System.out.println("Produs adaugat.");
        });
        b.addActionListener(e1 -> {
            System.out.println("Produs sters.");
        });
        c.addActionListener(e1 -> {
            System.out.println("Produs editat.");
        });
        d.addActionListener(e1 -> {
            System.out.println("Produs vandut.");
        });
        e.addActionListener(e1 -> {
            System.out.println("Produs retras.");
        });

        setSize(500, 400);
        setVisible(true);


    }

    public static void main(String[] args) {
        new Main();
    }
}



