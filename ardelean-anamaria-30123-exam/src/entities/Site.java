package entities;

import categories.Category;

import java.util.ArrayList;

public class Site {
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void initCategories(){}

    private ArrayList<Category> categories = new ArrayList<Category>();
}
