package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean running= true;
        Scanner scan = new Scanner(System.in);
        while(running){
            int optiune;
            System.out.println("Meniu de control: ");
            System.out.println("Adaugati un nou produs in categorie: 1 ");
            System.out.println("Stergeti un produs din categorie: 2 ");
            System.out.println("Editati un produs existent: 3 ");
            System.out.println("Puneti un produs la vanzare: 4");
            System.out.println("Retrageti un produs de la vanzare: 5");
            System.out.println("EXIT: orice altceva");
            System.out.println("Alegeti optiunea dorita: ");

            optiune=scan.nextInt();

            switch (optiune){
                case 1: System.out.println("A fost adaugat un nou produs in categorie.\n");
                    break;
                case 2: System.out.println("A fost sters un produs din categorie.\n");
                    break;
                case 3: System.out.println("A fost editat un produs din categorie.\n");
                    break;
                case 4: System.out.println("A fost pus la vanzare un produs din categorie.\n");
                    break;
                case 5: System.out.println("A fost retras un produs de la vanzare.\n");
                    break;
                default: System.out.println("Aceasta optiune nu este valida!\n");
                    running=false;
                    break;
            }
        }
    }
}
