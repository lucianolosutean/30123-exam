package categories;

import entities.Product;

public abstract class Category {
    private int id;
    private String name;
    public abstract void addProduct(Product p);
    public abstract void removeProduct(Product p);
    public abstract void updateProduct(Product p);
    public abstract Product getProduct(int id);
}
