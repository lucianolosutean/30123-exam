package entities;

import java.io.*;

public class Site {
    private String name;
    private String url;

    public Site(String name, String url) {
        this.name = name;
        this.url = url;
    }
    public void initCategories(){

    }

    public void save(String fileName) {
        try {
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Site-ul a fost salvat in:" + fileName);
        } catch (IOException e) {
            System.err.println("Site-ul nu poate fi salvat in fisier.");
            e.printStackTrace();
        }
    }

    public static Site read(String fileName) {
        Site s = null;
        try {
            ObjectInputStream o = new ObjectInputStream(new FileInputStream(fileName));
            s = (Site) o.readObject();
            System.out.println("Grupul a fost citit din fisierul:" + fileName);
        } catch (IOException e) {
            System.err.println("Grupul nu poate fi citit din fisier.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return s;
    }
}


