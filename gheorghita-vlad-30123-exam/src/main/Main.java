package main;

import categories.BookCategory;
import categories.ToyCategory;
import entities.Book;
import entities.Site;
import entities.Toy;

import java.util.Random;
import java.util.Scanner;

public class Main {
    private static void startStop() {
        System.out.println("1.Continue\n" +
                "2.Exit\n");
        System.out.print("Choose an option: ");
    }

    private static void categorySelection() {
        System.out.println("1.BookCategory\n" +
                "2.ToyCategory\n");
        System.out.print("Select a category: ");
    }

    private static void actionSelection() {
        System.out.println("1.Add Product\n" +
                "2.Remove Product\n" +
                "3.Show Product\n" +
                "4.Update Product\n");
        System.out.print("Select an action: ");
    }


    public static void main(String[] args) {
        boolean isOn = true;
        Scanner in = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Create Site:");
        System.out.print("Name: ");
        String name = in.next();
        System.out.print("Url: ");
        String url = in.next();
        Site site = new Site(name, url);
        while (isOn) {
            startStop();
            switch (in.nextInt()) {
                case 1:
                    categorySelection();
                    switch (in.nextInt()) {
                        case 1:
                            Book book = new Book();
                            System.out.println("Add a book:");
                            book.setId(random.nextInt(50));
                            System.out.print("Name: ");
                            book.setName(in.next());
                            book.setPrice(random.nextInt(100));
                            BookCategory bookCategory = new BookCategory(book);
                            actionSelection();
                            switch (in.nextInt()) {
                                case 1:
                                    Book book1 = new Book();
                                    System.out.print("Name: ");
                                    book1.setName(in.next());
                                    book1.setPrice(random.nextInt(100));
                                    book1.setId(random.nextInt(50));
                                    bookCategory.addProduct(book1);
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    break;
                            }
                            break;
                        case 2:
                            Toy toy = new Toy();
                            System.out.println("Add a toy:");
                            toy.setId(random.nextInt(50));
                            System.out.print("Name: ");
                            toy.setName(in.next());
                            toy.setPrice(random.nextInt(100));
                            ToyCategory toyCategory = new ToyCategory(toy);
                            actionSelection();
                            switch (in.nextInt()) {
                                case 1:
                                    Toy toy1=new Toy();
                                    System.out.print("Name: ");
                                    toy1.setName(in.next());
                                    toy1.setPrice(random.nextInt(100));
                                    toy1.setId(random.nextInt(50));
                                    toyCategory.addProduct(toy1);
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    break;
                            }
                            break;
                        default:
                            System.out.println("Wrong number! Choose another number!");
                            break;
                    }
                    break;
                case 2:
                    isOn = false;
                    break;
                default:
                    System.out.println("Wrong number! Choose another number!");
                    break;
            }
        }
    }
}
