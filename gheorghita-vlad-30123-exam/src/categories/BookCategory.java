package categories;

import entities.Book;
import entities.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class BookCategory extends Category {

    private ArrayList<Book> books = new ArrayList<Book>();

    public BookCategory(Book book){
        addProduct(book);
    }

    public void addProduct(Product p) {
        books.add((Book) p);
    }


    public void removeProduct(Product p) {
        books.remove(p);
    }


    public void updateProduct(Product p) {
        for (Book book : books) {
            if (book.equals(p)) {
                int nr;
                Scanner in = new Scanner(System.in);
                nr = in.nextInt();
                book.setId(nr);
                nr = in.nextInt();
                book.setPrice(nr);
                String name = in.next();
                book.setName(name);
            }
        }
    }


    public Product getProduct(int id) {
        for (Book book : books) {
            if (book.getId() == id) return book;
        }
        return null;
    }
}
