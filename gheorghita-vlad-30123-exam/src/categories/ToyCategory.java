package categories;

import entities.Product;
import entities.Toy;

import java.util.ArrayList;
import java.util.Scanner;

public class ToyCategory extends Category {

    private ArrayList<Toy> toys = new ArrayList<Toy>();

    public ToyCategory(Toy toy){
        addProduct(toy);
    }

    public void addProduct(Product p) {
        toys.add((Toy) p);
    }

    public void removeProduct(Product p) {
        toys.remove(p);
    }

    public void updateProduct(Product p) {
        for (Toy toy : toys) {
            int nr;
            Scanner in = new Scanner(System.in);
            nr = in.nextInt();
            toy.setId(nr);
            nr = in.nextInt();
            toy.setPrice(nr);
            String name = in.next();
            toy.setName(name);
        }
    }

    public Product getProduct(int id) {
        for (Toy toy : toys) {
            if (toy.getId() == id) return toy;
        }
        return null;
    }
}
