package entities;

import actions.RetireProduct;
import actions.SellProduct;

import java.util.Random;

public abstract class Product extends ProductionDetails implements SellProduct, RetireProduct {
    Random random = new Random();
    private boolean forSale = random.nextBoolean();
    private int categoryId = random.nextInt(50);

    public abstract void markForSale();

    public abstract void retireFromSale();

    public boolean isForSale() {
        return forSale;
    }

    public void setForsale(boolean forSale) {
        this.forSale = forSale;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;


    }
}
