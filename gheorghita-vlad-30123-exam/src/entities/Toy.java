package entities;

public class Toy extends Product {
    public void markForSale() {
        setForsale(true);
    }

    public void retireFromSale() {
        setForsale(false);
    }
}
