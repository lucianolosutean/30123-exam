package entities;

public class Book extends Product {
    public void markForSale() {
        setForsale(true);
    }

    public void retireFromSale() {
        setForsale(false);
    }

}
