package main;

import java.awt.*;
import javax.swing.*;

public class Main extends     JFrame
{
    private JTabbedPane tabbedPane;
    private JPanel panel1;
    private JPanel panel2;
    private JButton button;


    public Main()
    {

        setTitle( "Aplicatie" );
        setSize( 360, 240);
        setBackground( Color.gray );

        JPanel topPanel = new JPanel();
        topPanel.setLayout( new BorderLayout() );
        getContentPane().add( topPanel );

        // Create the tab pages
        createPage1();
        createPage2();

        // Create a tabbed pane
        tabbedPane = new JTabbedPane();
        tabbedPane.addTab( "Page 1", panel1 );
        tabbedPane.addTab( "Page 2", panel2 );
        topPanel.add( tabbedPane, BorderLayout.CENTER );
    }

    public void createPage1()
    {
        panel1 = new JPanel();
        panel1.setLayout( null );

        JLabel label1 = new JLabel( "Products.com" );
        label1.setBounds( 10, 15, 150, 20 );
        panel1.add( label1 );

        JLabel label2 = new JLabel( "Choose what you want to do:            In:" );
        label2.setBounds( 10, 30, 300, 20 );
        panel1.add( label2 );
    }

    public void createPage2()
    {
        panel2 = new JPanel();
        panel2.setLayout( new GridLayout( 3, 2 ) );
        panel2.add(new JLabel("Category"));
        panel2.add(new JLabel("Products"));
        panel2.add( new JLabel( "Book" ) );
        panel2.add( new TextArea() );
        panel2.add( new JLabel( "Toy" ) );
        panel2.add( new TextArea() );
    }



    // Main method to get things started
    public static void main( String args[] )
    {
        // Create an instance of the test application
        Main mainFrame  = new Main();
        mainFrame.setVisible( true );
    }
}