package entities;

import actions.RetireProduct;
import actions.SellProduct;

abstract public class Product extends ProductDetails implements SellProduct,RetireProduct {
    private boolean forSale;
    private int categoryId;

    abstract public void markForSale();
    abstract public void retireFromSale();

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }
}
