package entities;

public class Toy extends Product {
    @Override
    public void markForSale() {
        this.setForSale(true);
    }

    @Override
    public void retireFromSale() {
        this.setForSale(false);
    }
}
