package categories;

import entities.Product;
import entities.Toy;

import java.util.ArrayList;

public class ToyCategory extends Category {
    ArrayList<Toy> toys=new ArrayList<>();
    @Override
    public void addProduct(Product p) {
        toys.add((Toy)p);
    }

    @Override
    public void removeProduct(Product p) {
        toys.remove((Toy)p);
    }
    @Override
    public void updateProduct(Product p) {
        for(Toy i:toys){
            if(i.getName().equals(p.getName()))
                i.setName(p.getName());
            else
                System.out.println("Couldn't find product in toys!");
        }
    }

    @Override
    public Product getProduct(int id) {
        return toys.get(id);
    }
}
