package categories;

import entities.Book;
import entities.Product;

import java.util.ArrayList;

public class BookCategory extends Category {
    ArrayList<Book> books=new ArrayList<Book>();
    @Override
    public void addProduct(Product p) {
        books.add((Book)p);
    }

    @Override
    public void removeProduct(Product p) {
        books.remove((Book)p);
    }

    @Override
    public void updateProduct(Product p) {
        for(Book i:books){
            if(i.getName().equals(p.getName()))
                i.setName(p.getName());
            else
                System.out.println("Couldn't find product in books!");
        }
    }

    @Override
    public Product getProduct(int id) {
        return books.get(id);
    }
}
