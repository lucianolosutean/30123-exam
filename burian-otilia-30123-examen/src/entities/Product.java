package entities;

import actions.RetireProduct;
import actions.SellProduct;

abstract public class Product extends ProductDetails implements SellProduct, RetireProduct {
    private boolean forSale;
    private boolean categoryId;

    abstract public void retireForSale();

    abstract public void markForSale();

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public void setCategoryId(boolean categoryId) {
        this.categoryId = categoryId;
    }
}