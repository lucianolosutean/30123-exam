package categories;

import entities.*;

abstract public class Category {
    private int id;
    private String name;
    abstract public  void addProduct(Product p);
    abstract public  void removeProduct(Product p);
    abstract public  void updateProduct(Product p);
    abstract public  Product getProduct(int id);
}