package main;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean ruleaza= true;
        Scanner scan = new Scanner(System.in);
        while(ruleaza){
            int numar;
            System.out.println("Meniu : ");
            System.out.println("Adauga un nou produs in categorie --> 1 ");
            System.out.println("Sterge un produs din categorie    --> 2 ");
            System.out.println("Editeaza un produs existent       --> 3 ");
            System.out.println("Pune la vanzare un produs         --> 4");
            System.out.println("Retrage de pe piata un produs     --> 5");
            System.out.println("Inchide aplicatia                 --> oricare alt numar");
            System.out.println("Introduceti numarul: ");
            numar=scan.nextInt();

            switch (numar){
                case 1:
                    System.out.println("programul ar trebui sa adauge un nou produs in categorie");
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
                case 2:
                    System.out.println("programul ar trebui sa stearga un produs din categorie");
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
                case 3:
                    System.out.println("programul ar trebui sa editeze un produs din categorie");
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
                case 4:
                    System.out.println("programul ar trebui sa puna la vanzare un produs din categorie");
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
                case 5:
                    System.out.println("programul ar trebui sa retraga de la vanzare un produs din categorie");
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
                default: ruleaza=false;
                    break;
            }
        }
    }
}