package man_program;

import java.util.ArrayList;

public class Category {
    private int id;
    private String name;
    public ArrayList<Product> arrayList;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
        this.arrayList = new ArrayList<>();
    }

    public void addProduct(Product p) {
        arrayList.add(p);
    }

    public void removeProduct(Product p) {
        arrayList.remove(p);
    }

    public void updateProduct(Product p) {
        for (Product product : arrayList) {
            if (product == p) {
                p.setId(product.getId());
                p.setCategoryId(product.getCategoryId());
                p.setForSale(product.isForSale());
                p.setName(product.getName());
                p.setPrice(product.getPrice());

            }
        }
    }

    public Product getProduct(int id) {
        for (Product product : arrayList) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                '}';
    }

    public ArrayList<Product> getArrayList() {
        return arrayList;
    }
}

