package man_program;

import java.util.ArrayList;

public class Site {
    private String name;
    private String url;
    private ArrayList<Category> categories=new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public Site(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public void initCategories() {
        BookCategory bookCategory = new BookCategory(1, "Actiune");
        categories.add(bookCategory);

        ToyCategory toyCategory = new ToyCategory(2, "Lego");
        categories.add(toyCategory);
    }
}
