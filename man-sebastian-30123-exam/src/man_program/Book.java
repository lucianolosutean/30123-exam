package man_program;

public class Book  extends Product {
    public Book(int id, String name, int price, boolean forSale, int categoryId) {
        super(id, name, price, forSale, categoryId);
    }

    public void markForSale(){
        this.setForSale(true);
    }

    public void retireFromSale(){
        this.setForSale(false);
    }
}