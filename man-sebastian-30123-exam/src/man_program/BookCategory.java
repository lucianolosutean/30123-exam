package man_program;

import java.util.ArrayList;

public class BookCategory extends Category {
    private ArrayList<Product> arrayList;
    public BookCategory(int id, String name) {
        super(id, name);
        this.arrayList=new ArrayList();
    }

    public void addProduct(Product p) {
        arrayList.add(p);
    }

    public void removeProduct(Product p) {
        arrayList.remove(p);
    }

    public void updateProduct(Product p) {
        for (Product product : arrayList) {
            if (product == p) {
                p.setId(product.getId());
                p.setCategoryId(product.getCategoryId());
                p.setForSale(product.isForSale());
                p.setName(product.getName());
                p.setPrice(product.getPrice());

            }
        }
    }

    public Product getProduct(int id) {
        for (Product product : arrayList) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }
}
