package categories;

import entities.Product;
import entities.Toy;

import java.util.ArrayList;
import java.util.Scanner;

public class BookCategory extends Category {
    private ArrayList books = new ArrayList<>();

    public void addProduct(Product p) {
        books.add(p.getId(), p);
    }

    public void removeProduct(Product p) {
        books.remove(p);
    }

    public void updateProduct(Product p) {

        books.remove(p);
        Product p2 = new Product();
        p2.markForSale();
        System.out.println("Produs id: ");
        Scanner s1 = new Scanner(System.in);
        p2.setId(s1.nextInt());
        System.out.println("Name of the produs ");
        p2.setName(s1.next());
        System.out.println("Price of the product ");
        p2.setPrice(s1.nextInt());
        books.add(p2.getId(), p2);
    }

    public Product getProduct(int id) {
        return (Toy) books.get(id);
    }
}
