package categories;

import entities.Product;
import entities.Toy;

import java.util.ArrayList;
import java.util.Scanner;

public class ToyCategory extends Category {
    private ArrayList toys = new ArrayList<>();

    public void addProduct(Product p) {
        toys.add(p.getId(), p);
    }

    public void removeProduct(Product p) {
        toys.remove(p);
    }

    public void updateProduct(Product p) {

        toys.remove(p);
        Product p2 = new Product();
        p2.markForSale();
        System.out.println("Produs id: ");
        Scanner s1 = new Scanner(System.in);
        p2.setId(s1.nextInt());
        System.out.println("Name of the produs ");
        p2.setName(s1.next());
        System.out.println("Price of the product ");
        p2.setPrice(s1.nextInt());
        toys.add((Toy) p2);
    }

    public Product getProduct(int id) {
        return (Toy) toys.get(id);
    }
}

