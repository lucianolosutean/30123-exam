package entities;

import actions.RetireProduct;
import actions.SellProduct;

public class Product extends ProductDetails implements RetireProduct, SellProduct {
    private boolean forSale;
    private int categoryId;

    public void markForSale() {
        forSale = true;
    }

    public void retireFromSale() {
        forSale = false;
    }
}

