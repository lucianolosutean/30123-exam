package main;

import categories.BookCategory;
import categories.ToyCategory;
import entities.Product;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean istrue = true;
        int chose;
        int categorie;
        int id;
        ToyCategory ty = new ToyCategory();
        BookCategory bk = new BookCategory();
        while (istrue) {
            System.out.println("*****************Main*******************");
            System.out.println("* 1 Add a Product                      *");
            System.out.println("* 2 Remove a Product                   *");
            System.out.println("* 3 Update a product                   *");
            System.out.println("* 4 Mark for Sale                      *");
            System.out.println("* 5 Retire from Sale                    *");
            System.out.println("* 6 Exit                               *");
            System.out.println("****************************************");
            Scanner in = new Scanner(System.in);

            /*BufferedReader stdin = new BufferedReader(
                    new InputStreamReader(System.in));
            System.out.print("Enter a line:");
            System.out.println(stdin.readLine());*/

            chose = in.nextInt();
            switch (chose) {
                case 1:
                    Product p2 = new Product();
                    p2.markForSale();
                    System.out.println("Produs id: ");

                    p2.setId(in.nextInt());
                    System.out.println("Name of the produs ");
                    p2.setName(in.next());
                    System.out.println("Price of the product ");
                    p2.setPrice(in.nextInt());
                    System.out.println("What's the product category? (1-toy, 0-book)");
                    categorie = in.nextInt();
                    if (categorie == 1) {
                        ty.addProduct(p2);
                    } else {
                        bk.addProduct(p2);
                    }
                    break;
                case 2:
                    Product p1 = new Product();
                    p1.markForSale();
                    System.out.println("Produs id: ");
                    p1.setId(in.nextInt());
                    System.out.println("Name of the produs ");
                    p1.setName(in.next());
                    System.out.println("Price of the product ");
                    p1.setPrice(in.nextInt());
                    System.out.println("What's the product category? (1-toy, 0-book)");
                    categorie = in.nextInt();
                    if (categorie == 1) {
                        ty.removeProduct(p1);
                    } else {
                        bk.removeProduct(p1);
                    }
                    break;
                case 3:
                    Product p3 = new Product();
                    p3.markForSale();
                    System.out.println("Produs id: ");
                    p3.setId(in.nextInt());
                    System.out.println("Name of the produs ");
                    p3.setName(in.next());
                    System.out.println("Price of the product ");
                    p3.setPrice(in.nextInt());
                    System.out.println("What's the product category? (1-toy, 0-book)");
                    categorie = in.nextInt();
                    if (categorie == 1) {
                        ty.updateProduct(p3);
                    } else {
                        bk.updateProduct(p3);
                    }
                    break;
                case 4:
                    System.out.println("What's the product category? (1-toy, 0-book)");
                    categorie = in.nextInt();
                    System.out.println("What's product id");
                    id = in.nextInt();
                    if (categorie == 1) {
                        ty.getProduct(id).markForSale();
                    } else {
                        bk.getProduct(id).markForSale();
                    }
                    break;
                case 5:
                    System.out.println("What's the product category? (1-toy, 0-book)");
                    categorie = in.nextInt();
                    System.out.println("What's product id");
                    id = in.nextInt();
                    if (categorie == 1) {
                        ty.getProduct(id).retireFromSale();
                    } else {
                        bk.getProduct(id).retireFromSale();
                    }
                    break;
                case 6:
                    istrue = false;
                    break;
                default:
                    System.out.println("Valoare eronata");
                    break;

            }
        }
    }
}