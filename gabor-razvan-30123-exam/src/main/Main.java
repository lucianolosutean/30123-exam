package main;

import categories.Category;
import entities.Book;
import entities.Site;
import entities.Toy;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class Main extends JFrame {

    JMenuBar mb;
    JMenu f;

    JMenuItem b1,b2,b3,b4,b5;
    public Main(){
        mb = new JMenuBar();
        f = new JMenu("Magazin");


        b1= new JMenuItem("Adaugare nou produs in categorie");
        b2= new JMenuItem("Sterge un produs din categorie ");
        b3= new JMenuItem("Editare produs existent ");
        b4= new JMenuItem("Pune produs la vanzare ");
        b5= new JMenuItem("Retrage produs de la vanzare");
        mb.add(f);
        f.add(b1);
        f.add(b2);
        f.add(b3);
        f.add(b4);
        f.add(b5);
        setJMenuBar(mb);
        b1.addActionListener(e -> {System.out.println("Ai adaugat un produs");  });
        b2.addActionListener(e -> {System.out.println("Ai sters un produs"); });
        b3.addActionListener(e -> {System.out.println("Ai editat un produs"); });
        b4.addActionListener(e -> {System.out.println("Ai pus un produs la vanzare"); });
        b5.addActionListener(e -> {System.out.println("Ai retras un produs de la vanzare" ); });


        pack();
        setVisible(true);
    }



    public static void main(String[] args) throws IOException {
        new Main(); }}
