package entities;

import categories.Category;

import java.util.ArrayList;

public class Site {
	private String name;
	private String url;
	ArrayList<Category> categories = new ArrayList<>();

	public Site(String name, String url) {
		this.name = name;
		this.url = url;
	}

	public void initCategories() {

	}
}
