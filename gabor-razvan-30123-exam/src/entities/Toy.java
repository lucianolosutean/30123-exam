package entities;

public class Toy extends Product {
	@Override
	public void markForSale() {
		System.out.println("Pune la vanzare un produs");
	}

	@Override
	public void retireFromSale() {
		System.out.println("Retrage de la vanzare un produs");
	}
}
