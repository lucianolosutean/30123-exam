package entities;

import actions.RetireProduct;
import actions.SellProduct;

public abstract class Product extends ProductDetails implements SellProduct, RetireProduct {
	private boolean forSale;
	private int categoryId;

	abstract public void markForSale();

	abstract public void retireFromSale();
}
