package categories;

import java.util.ArrayList;

import entities.Product;
import entities.Toy;

public class ToyCategory extends Category {
	private ArrayList<Toy> toys = new ArrayList<>();

	public void addProduct(Product p) {
		toys.add((Toy) p);
	}

	public void removeProduct(Product p) {
		toys.remove((Toy) p);
	}

	public void updateProduct(Product p) {

	}

	public Product getProduct(int id) {
		return toys.get(id);
	}
}
