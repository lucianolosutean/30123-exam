package entities;

import actions.*;

public abstract class Product extends ProductDetails implements RetireProduct, SellProduct {
    private boolean forSale;
    private int categoryId;

    public abstract void markForSale();
    public abstract  void retireFromSale();

    public Product() {
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
