package categories;

import entities.Product;

public class BookCategory extends Category {
    @Override
    public void addProduct(Product p) {
        System.out.println("The product was added.");

    }

    @Override
    public void removeProduct(Product p) {
        System.out.println("The product was removed.");
    }

    @Override
    public Product getProduct(int id) {
        //cautand dupa id o sa returnam produsul corespunzator
        return null; //aici ar trebui returnata adresa unui produs
    }

    @Override
    public void updateProduct(Product p) {
        //cautand produsul in arrayList-ul cu produse o sa ii putem face update
        System.out.println("The product was updated.");
    }
}
